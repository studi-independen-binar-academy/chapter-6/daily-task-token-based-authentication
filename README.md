# Daily Task: Authentication & Bcrypt

Di dalam repository ini terdapat implementasi authentication dan penggunaan `bcryptjs` di dalamnya.

## Hasil Run Saat Register
![alt text](/public/assets/images/register.png)
## Hasil Run Saat Login
![alt text](/public/assets/images/login.png)